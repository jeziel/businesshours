// This was a new for me, I started with the WeekDay as
// union literal, but when I wanted to iterate over the
// days, my first question was whether or not that would
// be possible, turns out it's not and this is the best
// way:
// https://danielbarta.com/literal-iteration-typescript/
export const WeekDays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
] as const;

export type WeekDay = (typeof WeekDays)[number];

export interface TimePeriod {
  start_time: number;
  end_time: number;
}

export type HoursForDay = Record<WeekDay, TimePeriod[]>;
export type BusinessHours = Partial<HoursForDay>;
