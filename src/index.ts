import assert from 'assert';
import {BusinessHours} from './models';
import {transformBusinessHours} from './tools';

function compareArr(arr1: string[], arr2: string[]): boolean {
  return JSON.stringify(arr1) === JSON.stringify(arr2);
}

const input1: BusinessHours = {
  Monday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Tuesday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Wednesday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Thursday: [{start_time: 9, end_time: 15}],
  Friday: [{start_time: 9, end_time: 17}],
};

const output1 = [
  'Sunday: Closed',
  'Monday-Wednesday: 8:00am to 12:00pm, 1:00pm to 5:00pm',
  'Thursday: 9:00am to 3:00pm',
  'Friday: 9:00am to 5:00pm',
  'Saturday: Closed',
];

const times1 = transformBusinessHours(input1);
assert(compareArr(times1, output1), JSON.stringify(times1));
console.log(times1);

const input2: BusinessHours = {
  Monday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Tuesday: [
    {start_time: 9, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Wednesday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Thursday: [{start_time: 9, end_time: 15}],
  Friday: [{start_time: 9, end_time: 17}],
};

const output2 = [
  'Sunday: Closed',
  'Monday: 8:00am to 12:00pm, 1:00pm to 5:00pm',
  'Tuesday: 9:00am to 12:00pm, 1:00pm to 5:00pm',
  'Wednesday: 8:00am to 12:00pm, 1:00pm to 5:00pm',
  'Thursday: 9:00am to 3:00pm',
  'Friday: 9:00am to 5:00pm',
  'Saturday: Closed',
];

const times2 = transformBusinessHours(input2);
assert(compareArr(times2, output2), JSON.stringify(times2));
console.log(times2);

const input3: BusinessHours = {
  Tuesday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Wednesday: [
    {start_time: 8, end_time: 12},
    {start_time: 13, end_time: 17},
  ],
  Thursday: [{start_time: 9, end_time: 15}],
  Friday: [{start_time: 9, end_time: 17}],
};

const output3 = [
  'Sunday-Monday: Closed',
  'Tuesday-Wednesday: 8:00am to 12:00pm, 1:00pm to 5:00pm',
  'Thursday: 9:00am to 3:00pm',
  'Friday: 9:00am to 5:00pm',
  'Saturday: Closed',
];

const times3 = transformBusinessHours(input3);
assert(compareArr(times3, output3), JSON.stringify(times3));
console.log(times3);

const input4: BusinessHours = {};

const output4 = ['Sunday-Saturday: Closed'];

const times4 = transformBusinessHours(input4);
assert(compareArr(times4, output4), JSON.stringify(times4));
console.log(times4);

// I added this one, I was curious on a 3 element array.
const input5: BusinessHours = {
  Monday: [{start_time: 0, end_time: 0}], // probably 24 hours
  Tuesday: [
    {start_time: 8, end_time: 11},
    {start_time: 13, end_time: 16},
    {start_time: 20, end_time: 0},
  ],
  Wednesday: [
    {start_time: 8, end_time: 14},
    {start_time: 13, end_time: 17},
  ],
  Thursday: [{start_time: 9, end_time: 15}],
  Friday: [{start_time: 9, end_time: 17}],
};

const output5 = [
  'Sunday: Closed',
  'Monday: 12:00am to 12:00am',
  'Tuesday: 8:00am to 11:00am, 1:00pm to 4:00pm, 8:00pm to 12:00am',
  'Wednesday: 8:00am to 2:00pm, 1:00pm to 5:00pm',
  'Thursday: 9:00am to 3:00pm',
  'Friday: 9:00am to 5:00pm',
  'Saturday: Closed',
];

const times5 = transformBusinessHours(input5);
assert(compareArr(times5, output5), JSON.stringify(times5));
console.log(times5);
