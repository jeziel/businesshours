import {WeekDays, TimePeriod, BusinessHours} from './models';

const AM = 'am';
const PM = 'pm';

/**
 * Transform a numeric value that represents an hour of the day,
 * to a string of such hour.
 * 1 -> 1:00am
 * 23 -> 11:00pm
 * No validations are in place, a time like:
 * -1 -> -1:00am
 * 52 -> 40:00pm
 */
function timeString(time: number): string {
  let hour;
  let period;

  if (time === 0 || time === 12) {
    hour = 12;
    period = time < 12 ? AM : PM;
  } else if (time > 1 && time < 12) {
    hour = time;
    period = AM;
  } else if (time >= 12) {
    hour = time - 12;
    period = PM;
  }
  /*
  const midDay = 12;
  const diffHours = time === midDay ? 0 : 12;
  const [hour, period] =
    time >= midDay ? [time - diffHours, 'pm'] : [time, 'am'];
    */

  return `${hour}:00${period}`;
}

function timeToString(time: TimePeriod[] | undefined): string {
  if (time === undefined) {
    return 'Closed';
  }

  // TODO. Some sanitization can be done here.
  // The samples in the exercise don't overlap
  // and are presented in a sorted fashion, ealier
  // times come first.
  // Those were two assumptions that were taken
  // here.
  return time
    .map(t => {
      const start = timeString(t.start_time);
      const end = timeString(t.end_time);

      return `${start} to ${end}`;
    })
    .join(', ');
}

export function transformBusinessHours(hours: BusinessHours): string[] {
  let startDay = '';
  let previousTimes = '';
  const times: string[] = [];

  for (const day in WeekDays) {
    const weekDay = WeekDays[day];
    const currentTimes = timeToString(hours[weekDay]);

    // several things can cause an issue here:
    // - the start time is before the end time or viceversa
    //    resolution: bogus, nothing to do. Ignore? Throw exception?
    //      moving with: throwing an exception. The expected time should
    //        have a format of 24 hours, if for some reason the customer
    //        enters 9-2 to refer to 9-14, swapping would set the wrong
    //        times. If the user entered those values using a UI, the
    //        UI should have the same error notification.
    // - the number provided doesn't match a hour of the day
    //    resolution: we can "correct" to anything <0 -> 0, anything >23 -> 23
    //    resolution: logging, runtime exception.
    //      moving with: sanitizing
    // - there's an overlap on the time periods provided
    //    resolution: combine one period with another one
    //      9-13, 13-15 -> 9-15
    //      9-14, 13-15 -> 9-15
    //      9-13, 12-15 -> 9-15
    //      9-15, 12-15 -> 9-15
    //      9-12,  9-15 -> 9-15
    // - the hours are not ordered.
    //    resolution: once time segments have been identified, we can
    //      sort the items based on the start time.
    if (currentTimes === previousTimes) {
      times.pop();
      times.push(`${startDay}-${weekDay}: ${previousTimes}`);
    } else {
      startDay = weekDay;
      previousTimes = currentTimes;
      times.push(`${startDay}: ${previousTimes}`);
    }
  }

  return times;
}
